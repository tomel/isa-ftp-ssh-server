CC= gcc
CFLAGS= -g -Wall -D_POSIX_SOURCE

default: fakesrv

fakesrv:  main.o FTP.o argParse.o SSH.o Common.o
	$(CC) $(CFLAGS) -pthread $^ -lssh -o $@

main.o:  main.c main.h struct.h
	$(CC) $(CFLAGS) -c main.c

FTP.o:  FTP.c FTP.h struct.h
	$(CC) $(CFLAGS) -pthread -c FTP.c -lpthread

argParse.o:  argParse.c argParse.h struct.h
	$(CC) $(CFLAGS) -c argParse.c

SSH.o:  SSH.c SSH.h struct.h
	$(CC) $(CFLAGS) -pthread -lssh -c SSH.c -lpthread

Common.o:  Common.c Common.h Common.h
	$(CC) $(CFLAGS) -pthread -lssh -c Common.c

clean:
	rm -f fakesrv  *.o xkello00.tar.gz

pack:
	tar -pczf xkello00.tar.gz Makefile manual.pdf README *.h *.c
