#ifndef FTP_h
#define FTP_h
#include "main.h"
#include "struct.h"

// spravy posielane clientovy
#define msgWelcome "220 Welcom on server xkello00\n"
#define msgWrongPass "530 wrong passwd\n"
#define msgRecivedUSER "331 need passwd\n"
#define msgNotCommand "cant recognize command\n"
#define msgEndCommunication "221 BYE\n"
#define MSG_START 5

void sigHandler(int dummy);

// funkcia obsluhuje klienta
// dostane okazovatel na spojenie
void *ClientWaiting(void *arg);

// pripravy server na komunikaciu
// po nadviazani spojenia vytvori vlakno
// a zavola ClientWaiting ktory sa o neho postara
bool FTPserver();

#endif