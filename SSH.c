#include "SSH.h"
// Global aby mohol pracovat program s nimi aj pri priati signalu

static ssh_bind sBind;
//static ssh_session session;
extern pthread_attr_t attr;
extern argStruct argMain;
extern char fileName[50];
/*
// uloz IP adresu do struct connection
void get_client_ip(struct connection *c) {
	socklen_t len;
	struct sockaddr_storage addr;
	len = sizeof(addr);
	getpeername(ssh_get_fd(session), (struct sockaddr*)&addr, &len);
// pracuje s IPv4 aj IPv6
	if (addr.ss_family == AF_INET) {
		struct sockaddr_in *s = (struct sockaddr_in *)&addr;
		inet_ntop(AF_INET, &s->sin_addr, c->client_ip, INET6_ADDRSTRLEN);
	} else { // AF_INET6
		struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
		inet_ntop(AF_INET6, &s->sin6_addr, c->client_ip, INET6_ADDRSTRLEN);
	}
}
*/
void sigHandlerSSH(int dummy)
{
	ssh_bind_free(sBind);
	ssh_finalize();
	exit(EXIT_SUCCESS);
}

void *ClientWaitingSSH(void *arg)
{
	signal(SIGPIPE, SIG_IGN);

	struct argData *argData;
	argData = (struct argData *)arg;
	ssh_message message;
	ssh_session session;
	session = argData->session;




// zvysi pocet pripojenych klientov cez MUTEX
		if (!NumOfClient(+1))
		{
			handle_error("SERVER: Too a lot of connections");
			ssh_disconnect(session);
			free(argData);
			return 0;
		}


// vymena kluca
	if (ssh_handle_key_exchange(session))
	{
		handle_error("SERVER_SSH: ERROR exchanging key");
	// zniz pocitadlo pripojenych klientov kedze sa vyskytol error a ukonci spojenie
		NumOfClient(-1);
		ssh_disconnect(session);
		free(argData);
		if (DEBUG) printf("Exiting child before WHILE\n");
		return 0;
	}
	// vytvor a inicializuj strukturu pre zapis
	ZapisStruct ZapisS;
	bzero(ZapisS.user, sizeof(ZapisS.user));
	bzero(ZapisS.pass, sizeof(ZapisS.pass));
	get_client_ip(ssh_get_fd(session), ZapisS.IPadd);
	// ziska aktualny cas zadania pripojenia
	getTime(&ZapisS);

	if (DEBUG)
		printf("Successful key exchange.\n");

// primaj a odpovedaj na spravy cez while
	uint passMax = argMain.passMax;
	while (1) {

// spojenie bolo ukoncene
		if ((message = ssh_message_get(session)) == NULL)
			handle_errorBREAK("message_get error")

// uzivatel sa snazi prihlasit heslom
		if (ssh_message_subtype(message) == SSH_AUTH_METHOD_PASSWORD) {

			// ziska aktualny cas zadania hesla
			getTime(&ZapisS);
			// naprni strukturu loginom a heslom
			strcpy(ZapisS.user, ssh_message_auth_user(message));
			strcpy(ZapisS.pass, ssh_message_auth_password(message));
			// zapis do suboru cez mutex
			if (!zapis(ssh_get_fd(session), &ZapisS))
				break;

			passMax--; // pocet pokusov o zadanie hesla, lokalna premenna
			if (passMax == 0)
			{
				if (DEBUG)
					printf("SERVER_SSH: prekroceny limit pokusov o zadanie hesla\n");
				break;
			}
		}
		else {
// uzivatel sa chce prihlasit inak
			if (DEBUG) { fprintf(stderr, "Not a password authentication attempt.\n"); }
		}
// odpovec uzivatelovi automaticky a vymaz spravu
		ssh_message_reply_default(message);
		ssh_message_free(message);
	} // end While


	// zniz pocitadlo pripojenych klientov cez mutex
	NumOfClient(-1);
	// ak nezadal heslo vypis zaznam o pripojeni
	if (ZapisS.pass[0] == 0)
		zapis(ssh_get_fd(session), &ZapisS);

	if (DEBUG) printf("Exiting child.\n");
	ssh_disconnect(session);
	free(argData);
	return 0;
}

bool SSHserver()
{
	signal(SIGCHLD, sigHandlerSSH);
	signal(SIGINT, sigHandlerSSH);

// nastav a vytvor spojenie
	sBind=ssh_bind_new();
	ssh_bind_options_set(sBind, SSH_BIND_OPTIONS_BINDADDR, argMain.IPadd);
	ssh_bind_options_set(sBind, SSH_BIND_OPTIONS_BINDPORT, &argMain.PortNum);
	ssh_bind_options_set(sBind, SSH_BIND_OPTIONS_HOSTKEY, "ssh-rsa");
	ssh_bind_options_set(sBind, SSH_BIND_OPTIONS_RSAKEY, argMain.RSAkey);

// LISTEN
	if (ssh_bind_listen(sBind) < 0) {
		printf("Error listening to socket: %s\n",ssh_get_error(sBind));
		return -1;
	}
	if (DEBUG)
		printf("SERVER: Listening on port %d.\n", argMain.PortNum);


	pthread_t thread_id; // priprava na vytvorenie vlakna
	struct argData *argData; // premenna posielana ako argument vlaknu


	/* Loop forever, waiting for and handling connection attempts. */
	while (1) {

		// vytvori argumenty na poslanie do funkcie, uvolnuje ich vlakno samo
			argData = (struct argData*)malloc(sizeof(*argData));
			if (argData == NULL)
				fatal_error("SERVER_SSH: cant malloc memory");
			argData->session = ssh_new();

		//session=ssh_new();
		if (ssh_bind_accept(sBind, argData->session) == SSH_ERROR) {
			fatal_error("SERVER_SSH: error on bind")
		}
		if (DEBUG)
				printf("SERVER_SSH: ACCEPTED CONNECTION\n");



		// vytvor vlakno a zavolaj obsluznu funkciu
			if (pthread_create(&thread_id, &attr, &ClientWaitingSSH, argData) != 0)
				fatal_error("SERVER_SSH: ERROR pthread_create");
	}

	ssh_bind_free(sBind);
	ssh_finalize();
	return true;
}