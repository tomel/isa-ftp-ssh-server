#ifndef mainH
#define mainH


// name:      login
// password:  encripted
// uid:       ID
// gid:       login groupt id
// gecos:     general info
// homedir:   home dir
// shell      login shell
//
// PACKET
// flags with index          l-i   name
// L  -   U   G   N   H   S  opt   name
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <time.h>
#include <ctype.h>
#include <signal.h>
#include <pthread.h>
#include <stdbool.h>
#include <strings.h>
#include <libssh/libssh.h>
#include <libssh/server.h>

#include <getopt.h>
#include <errno.h>
#include <sys/wait.h>

#include "struct.h"
#include "Common.h"
#include "argParse.h"
#include "FTP.h"
#include "SSH.h"

#define uint unsigned int
#define DEBUG 0


#define fatal_error(msg) \
        { fprintf(stderr, "%s\n", msg); sigHandler(0); }
#define handle_error(msg) \
        { fprintf(stderr, "%s\n", msg); }
#define handle_errorBREAK(msg) \
        { fprintf(stderr, "%s\n", msg); break;}
#define NUM_OF_ARG_TYPE 7
#define HELP_MSG "SSH/FTP server Tomas Kello\r\n \
date: 20.10.2015\r\n \
email: xkello00@stud.fit.vutbr.cz\r\n \
projekt do predmetu ISA\r\n \
spustanie prepinacmi\r\n \
Povinný parametr -m definuje režim serveru: ftp nebo ssh.\r\n\r\n \
POUZITIE:\r\n \
-m [mod FTP/SSH]\r\n \
-a [IP adresa]\r\n \
-p [TCP portu]\r\n \
-l [log subor]\r\n \
-r [RSA kluc]\r\n \
-c [int] max pocet pripojenych uzivatelov  (Default 10)\r\n \
-t [int] max pocet pokusov o zadanie hesla (Default 3)\r\n\r\n"

int main (int argc, char *argv[]);

#endif