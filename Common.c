#include "Common.h"
extern char fileName[50];
extern NumOfConn ConnNum;
extern argStruct argMain;


bool NumOfClient(int i)
{
	pthread_mutex_lock(&ConnNum.mutex);
	ConnNum.num = ConnNum.num - i;
	if (ConnNum.num < 0)
	{
		// vrat operaciu
		ConnNum.num = ConnNum.num + i;
		pthread_mutex_unlock(&ConnNum.mutex);
		return false;
	}
	if(DEBUG)
		printf("ostava: %d pripojeni\n", ConnNum.num);
	pthread_mutex_unlock(&ConnNum.mutex);
	return true;
}

// uloz IP adresu do struct connection
void get_client_ip(int sock, char *IPadd) {
	socklen_t len;
	struct sockaddr_storage addr;
	len = sizeof(addr);
	getpeername(sock, (struct sockaddr*)&addr, &len);
// pracuje s IPv4 aj IPv6
	if (addr.ss_family == AF_INET) {
		struct sockaddr_in *s = (struct sockaddr_in *)&addr;
		inet_ntop(AF_INET, &s->sin_addr, IPadd, INET_ADDRSTRLEN);
	} else { // AF_INET6
		struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
		inet_ntop(AF_INET6, &s->sin6_addr, IPadd, INET6_ADDRSTRLEN);
	}
}

void getTime(ZapisStruct *ZapisS)
{
		time_t t;
		time(&t);
		struct tm *info;
		info = localtime( &t);
		strftime(ZapisS->timeOut, MAXTIME, "%Y-%m-%d %H:%M:%S", info);
}
bool zapis(int s, ZapisStruct *ZapisS)
{
	// vylucny zapis do suboru
	pthread_mutex_lock(&ConnNum.mutexFile);
	
		FILE *f;
		if ((f = fopen(fileName, "a")) == NULL)
		{
			handle_error("SERVER_SSH: Open log file");
			return false;
		}

		if (DEBUG)
			printf("%s %s %s %s %s\n", argMain.ssh?"SSH":"FTP", ZapisS->timeOut, ZapisS->IPadd, ZapisS->user, ZapisS->pass);
		if (ZapisS->pass[0] == 0)
		{
			if(ZapisS->user[0] == 0)
			{
				fprintf(f, "%s %s %s\n", argMain.ssh?"SSH":"FTP", ZapisS->timeOut, ZapisS->IPadd);
			}
			else
				fprintf(f, "%s %s %s %s\n", argMain.ssh?"SSH":"FTP", ZapisS->timeOut, ZapisS->IPadd, ZapisS->user);
		}
		else
			fprintf(f, "%s %s %s %s %s\n", argMain.ssh?"SSH":"FTP", ZapisS->timeOut, ZapisS->IPadd, ZapisS->user, ZapisS->pass);
		fclose(f);
	
	// koniec zapisu do suboru
	pthread_mutex_unlock(&ConnNum.mutexFile);
	return true;
}