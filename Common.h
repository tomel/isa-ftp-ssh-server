#ifndef COMMON_H
#define COMMON_H
#include "main.h"


// nastavi hodnotu TimeOut v strukture zapisStruct
void getTime(ZapisStruct *Zapis);

// z info zo sock skopiruje IP adresu do IPadd
void get_client_ip(int sock, char *IPadd);

// zapis do suboru cez mutex
// s spojenie odkail si zobere IPadresu
// user - meno ktore sa vypise
// pass - heslo ktore sa vypise
bool zapis(int s, ZapisStruct *Zapis);

// pridava alebo odobera pripojenych klientov
// ak sa klient nemoze pripojit vracia false
bool NumOfClient(int i);

#endif