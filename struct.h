#ifndef struct_h
#define struct_h

#define MAXTIME 100

typedef struct {
  pthread_mutex_t mutexFile;
  pthread_mutex_t mutex;
  int num;
} NumOfConn;

typedef struct 
{
	char IPadd[INET6_ADDRSTRLEN];
	char RSAkey[50];
	bool ssh;
	int ai_family;
	unsigned int PortNum;
	unsigned int connectionsMax;
	unsigned int passMax;
}argStruct;

struct argData
{
  int t;
  ssh_session session;
};

typedef struct 
{
	char timeOut[MAXTIME];
	char user[200];
	char pass[200];
	char IPadd[INET6_ADDRSTRLEN];
} ZapisStruct;


#endif