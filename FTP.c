#include "FTP.h"

extern char fileName[50];
extern pthread_attr_t attr;
extern argStruct argMain;
int s;

void Client_KILL(int dummy)
{
	if (DEBUG)
		printf("Child was killed\n");
}

void *ClientWaiting(void *arg)
{
	signal(SIGPIPE, SIG_IGN);
	signal(SIGCHLD, Client_KILL);
	struct argData *tmp;
	tmp = (struct argData *)arg;
	int t = tmp->t;
	struct timeval tv;
	tv.tv_sec = 10;
	tv.tv_usec = 0;
	setsockopt(t, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv));




		
// zvys pocet pripojenych klientov cez mutex
	if(!NumOfClient(+1))
	{
		if ( write(t, msgEndCommunication, sizeof(msgEndCommunication) ) < 0 )  // echo message back
			handle_error("SERVER: error on write 1"); /*  write error */
		fprintf(stderr, "SERVER: Too a lot of connections\n");
		free(arg);
		return 0;
	}
	
	ZapisStruct ZapisS;
	bzero(ZapisS.user, sizeof(ZapisS.user));
	bzero(ZapisS.pass, sizeof(ZapisS.pass));
	get_client_ip(t, ZapisS.IPadd);

	// ziska aktualny cas pripojenia
	getTime(&ZapisS);

	// SEND INVITE MESSAGE
	if ( write(t, msgWelcome, sizeof(msgWelcome) ) < 0 )  // echo message back
		handle_error("SERVER: error on write 2"); //  write error

	if(DEBUG)
		printf("Welcome message was send\n");
	char msg[250];
	uint8_t msglen = 0;

	while(1)
	{
			// vynuluj spravu
			bzero(msg,sizeof(msg));

			// precitaj spravu od klienta
			if ((msglen = read(t, msg, sizeof(msg))) <0)
				handle_errorBREAK("SERVER: error on read");
			msg[msglen-2] = 0; // ukonci spravu bez \r a \n
			msg[4] = 0; // rozdel spravu na COMMAND a ARGUMENTS

			// uzivatel poslal prikaz ukoncenia spojenia
			if (!strcasecmp(msg, "QUIT"))
				{
					if ( write(t, msgEndCommunication, sizeof(msgEndCommunication) ) < 0 )  // echo message back
						handle_errorBREAK("SERVER: error on write 3"); //  write error
					break;
				}

			// uzivatel zadal heslo
			else if (!strcasecmp(msg, "PASS"))
			{
				// ziska aktualny cas zadania hesla
				getTime(&ZapisS);
				// vlozi heslo do struktury
				strcpy(ZapisS.pass, &msg[MSG_START]);
				// vylucny pristup k zapisu do suboru

				// odpovedaj o nespravnom hesle
				if ( write(t, msgWrongPass, sizeof(msgWrongPass) ) < 0 )
					handle_errorBREAK("SERVER: error on write 4"); /*  write error */
				break;
			}

			// uzivatel zadal login, ulozi ho do premennej user
			else if (!strcasecmp(msg, "USER"))
			{
				// ak uz bol zadany login bez hesla
				// (ak bol s heslom tak uz je vypisany)
				if(ZapisS.user[0] != 0 && ZapisS.pass != NULL)
					zapis(t, &ZapisS);
				// ziska aktualny cas zadania loginu
				getTime(&ZapisS);
				// uloz si login zo spravy
				strcpy(ZapisS.user, &msg[MSG_START]);
				if ( write(t, msgRecivedUSER, sizeof(msgRecivedUSER) ) < 0 )  /* echo message back */
					handle_errorBREAK("SERVER: error on write 5"); /*  write error */
			}

			// nerozproznany prikaz
			else
				if ( write(t, msgNotCommand, sizeof(msgNotCommand) ) < 0 )  /* echo message back */
					handle_errorBREAK("SERVER: client disconected"); /*  write error */

		}

		// zniz pocitadlo pripojenych klientov
		NumOfClient(-1);

		// zapis do Log suboru
		zapis(t, &ZapisS);

		// uzavri spojenie
		if (close(t) < 0)
			handle_error("SERVER: error on close connection");

		// uvolni argumenty
		free(arg);

		if(DEBUG)
			printf("zavrete spojenie\n");
		return NULL;
}

void sigHandler(int dummy)
{
  if (pthread_attr_destroy(&attr) != 0)
    handle_error("Error: pthread_attr_destroy\n");
  if(s != 0)
    if(close(s) <0)
      handle_error("Error: close socket\n");
  printf("SERVER shut down\n");
  exit(EXIT_SUCCESS);
}

bool FTPserver()
{
	signal(SIGTERM, sigHandler);
	signal(SIGINT, sigHandler);
	signal(SIGPIPE, SIG_IGN);

	/*
// zisti typ adresy
	struct addrinfo inIP, *outIP;
	memset(&inIP, 0, sizeof(inIP));
	inIP.ai_family = PF_UNSPEC;
	inIP.ai_flags = AI_NUMERICHOST;
	if (getaddrinfo(argMain.IPadd, NULL, &inIP, &outIP))
		fatal_error("cant resolve IP")
*/

// IPv4
	if (argMain.ai_family == AF_INET)
	{
		// SOCKET AF_INET==IPv4 SOCK_STREAM==TCP
		if ( (s = socket(AF_INET, SOCK_STREAM, 0 ) ) < 0)
			fatal_error("IPv4 error on socket");
		// nastav adresy IPv4
		struct sockaddr_in sin;
		bzero(&sin, sizeof(sin));
		sin.sin_family = AF_INET; // IPv4 family
		sin.sin_port = htons(argMain.PortNum);
		inet_pton(AF_INET, argMain.IPadd, &sin.sin_addr); // IPv4 adresa

	// BIND IPv4
		if (bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0 )
			fatal_error("IPv4 error on bind");
	
	}
	else if (argMain.ai_family == AF_INET6)
	{
// IPv6
		// SOCKET AF_INET6==IPv6 SOCK_STREAM==TCP
		if ( (s = socket(AF_INET6, SOCK_STREAM, 0 ) ) < 0)
			fatal_error("IPv6 error on socket");

		// nastav adresy IPv6
		struct sockaddr_in6 sin6;
		bzero(&sin6, sizeof(sin6));
		sin6.sin6_family = AF_INET6;  //IPv6 rodina
		sin6.sin6_port = htons(argMain.PortNum); 
		inet_pton(AF_INET6, argMain.IPadd, &sin6.sin6_addr); //IPv6 adresa
		sin6.sin6_addr = in6addr_any;

	// BIND IPv6
		if (bind(s, (struct sockaddr *)&sin6, sizeof(sin6)) < 0 )
			fatal_error("IPv6 error on bind"); /* bind error */
	}
	else
		fatal_error("SERVER: not IPv4/IPv6 address")

	if (DEBUG)
		printf("starting LISTENING\n");

// LISTEN
		if (listen(s, argMain.connectionsMax)) // max connections
			fatal_error("error on listen"); /* listen error*/


	int t, soutlen;
	struct sockaddr sout;
	struct argData *tmp;

	pthread_t thread_id;

	// cyklus prima spojenia
	// pre kazde spojenie vytvori vlakno a zavola funkciu na obsluhu
	while(1)
	{
		// primi spojenie
			soutlen = sizeof(sout);
			if ( (t = accept(s, &sout, (socklen_t*)&soutlen) ) < 0 )
				fatal_error("error on accept");  /* accept error */
			if (DEBUG)
				printf("ACCEPTED CONNECTION\n");

		// vytvori argumenty na poslanie do funkcie, uvolni si ich vlakto
			tmp = (struct argData*)malloc(sizeof(*tmp));
			if (tmp == NULL)
				fatal_error("SERVER: cant malloc memory");
			tmp->t = t;

		// vytvor vlakno a zavolaj obsluznu funkciu
			if (pthread_create(&thread_id, &attr, &ClientWaiting, tmp) != 0)
				fatal_error("ERROR pthread_create");
	}
	return true;
}