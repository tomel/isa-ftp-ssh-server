#include "argParse.h"

extern char fileName[50];
extern argStruct argMain;

void argParse(int argc, char *argv[])
{
  int opt, argCount = 0;

  // odchytenie vsetkych parametrov
  while ((opt = getopt(argc, argv, "a:m:l:p:r:c:t:")) != -1) {
    switch (opt) {
      case 'a':
        argCount += 1;
        strcpy(argMain.IPadd, optarg);

        // zisti typ adresy
          struct addrinfo inIP, *outIP;
          memset(&inIP, 0, sizeof(inIP));
          inIP.ai_family = PF_UNSPEC;
          inIP.ai_flags = AI_NUMERICHOST;
          if (getaddrinfo(argMain.IPadd, NULL, &inIP, &outIP))
            fatal_error("cant resolve IP")

          argMain.ai_family = outIP->ai_family; // nastavi AF_INET/AF_INET6

        break;
      case 'm':
        argCount += 10;
        if (!strcasecmp(optarg, "ftp"))
          argMain.ssh = false;
        else
          if (!strcasecmp(optarg, "ssh"))
            argMain.ssh = true;
          else
          {
            printf(HELP_MSG);
            fatal_error("SERVER: zly mod (mozne len ftp/ssh)");
          }
        break;
      case 'l':
        argCount += 100;
        strcpy(fileName, optarg);
        break;
      case 'p':
        argCount += 1000;
        argMain.PortNum = atoi(optarg);
        break;
      case 'r':
        argCount += 10000;
        strcpy(argMain.RSAkey, optarg);
        break;
      case 'c':
        argMain.connectionsMax = atoi(optarg);
        break;
      case 't':
        argMain.passMax = atoi(optarg);
        break;
      default:
        printf(HELP_MSG);
        fatal_error("neocakavany argument");
        break;
    }
  }
  // kontrola ci boli parametre a, m, l, p zadane prave RAZ
  if (argCount %10000 != 1111)
  {
    printf(HELP_MSG);
    fatal_error("Not all required arg set (a, m, l, p)");
  }

  // ak je server SSH kontroluje zadanie RSA
  if (argMain.ssh)
  {
    if (argCount != 11111)
    {
      printf(HELP_MSG);
      fatal_error("SSH need RSA key, -r param");
    }
  }
}
// END OF ARGUMENTS PROCESS