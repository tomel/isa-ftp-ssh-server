#include "main.h"

// shared variable
char fileName[50];
argStruct argMain;
pthread_attr_t attr;
NumOfConn ConnNum;




int main (int argc, char *argv[])
{
// nastavy default hodnoty
  argMain.ssh = false;
  argMain.PortNum = 0;
  argMain.connectionsMax = 10;
  argMain.passMax = 3;

// pripravy parametre programu
// naplna globalnu strukturu argMain
  argParse( argc, argv);

// pripravy mutex
  ConnNum.num = argMain.connectionsMax;
  pthread_mutex_init (&ConnNum.mutex , NULL);
  pthread_mutex_init (&ConnNum.mutexFile , NULL);

// Inicializuj vytvaranie vlakien
  if (pthread_attr_init(&attr) != 0)
    fatal_error("pthread_attr_init");

  if(argMain.ssh)
    SSHserver();
  else
    FTPserver();

  return 0;
}
